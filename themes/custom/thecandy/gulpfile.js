'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

gulp.task('sass', function(){
    return gulp.src('src/scss/**/*.scss')
      .pipe(sass()) 
      .pipe(gulp.dest('src/css'))
});

gulp.task('watch', function(){
    gulp.watch('src/scss/**/*.scss', gulp.series(['sass']));
})


const { src, dest, series, parallel } = require('gulp');
const minify = require("gulp-minify");
const rename = require("gulp-rename");
const cleanCSS = require('gulp-clean-css');
const del = require('del');

const clean = () => del([ 'dist' ]);

function styles() {

    return src('src/css/*.css', { allowEmpty: true }) 
        .pipe(cleanCSS())
        .pipe(rename({
          suffix: '.min'
        }))
        .pipe(dest('dist/css'))
}

function fonts() {

    return src('src/fonts/roboto/*.css', { allowEmpty: true }) 
        .pipe(cleanCSS())
        .pipe(rename({
          suffix: '.min'
        }))
        .pipe(dest('dist/fonts'))
}

function scripts() {

    return src('src/js/*', { allowEmpty: true }) 
        .pipe(minify({noSource: true}))
        .pipe(dest('dist/js'))
}

const build = series(clean, parallel(styles, fonts, scripts));

exports.styles = styles;
exports.styles = fonts;
exports.scripts = scripts;
exports.clean = clean;
exports.build = build;

exports.default = build;

